package com.hrhx.springboot.mysql.jpa;

import com.hrhx.springboot.domain.ImookCourse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImookCourseRepository extends JpaRepository<ImookCourse, Long> {
}
