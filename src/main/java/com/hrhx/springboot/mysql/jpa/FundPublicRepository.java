package com.hrhx.springboot.mysql.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hrhx.springboot.domain.FundPublic;
/**
 * 
 * @author duhongming
 *
 */
public interface FundPublicRepository extends JpaRepository<FundPublic, Long> {

}
